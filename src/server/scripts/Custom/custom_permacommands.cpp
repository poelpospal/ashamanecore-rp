#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Chat.h"
#include "Player.h"
#include "RBAC.h"
#include "World.h"
#include "WorldSession.h"
#include "MiscPackets.h"
#include "DatabaseEnv.h"

class permacommands_script : public CommandScript
{
public:
	permacommands_script() : CommandScript("permacommands_script") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> pmorphOrPscaleTable =
		{
			{ "morph",     rbac::RBAC_PERM_COMMAND_MORPH,			false, &HandlePermaMorphCommand,    "", },
			{ "scale",     rbac::RBAC_PERM_COMMAND_MODIFY_SCALE,    false, &HandlePermaScaleCommand,	"", },
		};

		static std::vector<ChatCommand> permaTable =
		{
			{ "perma",     rbac::RBAC_PERM_COMMAND_MORPH,			false, NULL,						"", pmorphOrPscaleTable },
		};

		static std::vector<ChatCommand> commandTable =
		{
			{ "modify",     rbac::RBAC_PERM_COMMAND_MODIFY,			false, NULL,						"", permaTable },
		};
		return commandTable;
	}

	
};


void AddSC_permacommands_commandscript()
{
	new permacommands_script();
}
