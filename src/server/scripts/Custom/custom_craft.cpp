#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Chat.h"
#include "Player.h"
#include "RBAC.h"
#include "World.h"
#include "WorldSession.h"
#include "MiscPackets.h"
#include "Item.h"
#include "Log.h"
#include "Language.h"
#include "Config.h"


class custom_craft : public CommandScript
{
public:
    custom_craft() : CommandScript("custom_craft") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> itemTable =
        {
            { "craft",			rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleCraftItemCommand,            "" },
            { "info",			rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleItemInfoCommand,             "" },
            { "upgrade",	    rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleItemUpgradeCommand,          "" },
        };

        static std::vector<ChatCommand> commandTable =
        {
            { "item",           rbac::RBAC_PERM_COMMAND_SAVE,      false, NULL,                             "", itemTable },
        };
        return commandTable;
    }

    static bool HandleItemInfoCommand(ChatHandler* handler, char const* args)
    {
        /*
        uint32 itemId = 0;

        if (args[0] == '[')                                        // [name] manual form
        {
            char const* itemNameStr = strtok((char*)args, "]");

            if (itemNameStr && itemNameStr[0])
            {
                std::string itemName = itemNameStr + 1;
                auto itr = std::find_if(sItemSparseStore.begin(), sItemSparseStore.end(), [&itemName](ItemSparseEntry const* sparse)
                {
                    for (uint32 i = 0; i < MAX_LOCALES; ++i)
                        if (itemName == sparse->Display->Str[i])
                            return true;
                    return false;
                });

                if (itr == sItemSparseStore.end())
                {
                    handler->PSendSysMessage(LANG_COMMAND_COULDNOTFIND, itemNameStr + 1);
                    handler->SetSentErrorMessage(true);
                    return false;
                }

                itemId = itr->ID;
            }
            else
                return false;
        }
        else                                                    // item_id or [name] Shift-click form |color|Hitem:item_id:0:0:0|h[name]|h|r
        {
            char const* id = handler->extractKeyFromLink((char*)args, "Hitem");
            if (!id)
                return false;
            itemId = atoul(id);
        }

        std::vector<int32> bonusListIDs;
        char const* bonuses = strtok(NULL, " ");

        // semicolon separated bonuslist ids (parse them after all arguments are extracted by strtok!)
        if (bonuses)
        {
            Tokenizer tokens(bonuses, ';');
            for (char const* token : tokens)
                bonusListIDs.push_back(atoul(token));
        }

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->getSelectedPlayer();
        playerTarget = player; /// TODO: Revork this

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (itemTemplate->GetInventoryType() == INVTYPE_TRINKET || itemTemplate->GetInventoryType() == INVTYPE_FINGER || itemTemplate->GetInventoryType() == INVTYPE_RELIC || itemTemplate->GetContainerSlots() > 0)
        {
            handler->PSendSysMessage("You can't craft that item.");
            return true;
        }
        */
        handler->PSendSysMessage("Work in progress.");
        return true;
    }

    static bool HandleItemUpgradeCommand(ChatHandler* handler, char const* args)
    {
        handler->PSendSysMessage("Work in progress.");
        return true;
    }

    static bool HandleCraftItemCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        uint32 itemId = 0;

        if (args[0] == '[')                                        // [name] manual form
        {
            char const* itemNameStr = strtok((char*)args, "]");

            if (itemNameStr && itemNameStr[0])
            {
                std::string itemName = itemNameStr + 1;
                auto itr = std::find_if(sItemSparseStore.begin(), sItemSparseStore.end(), [&itemName](ItemSparseEntry const* sparse)
                {
                    for (uint32 i = 0; i < MAX_LOCALES; ++i)
                        if (itemName == sparse->Display->Str[i])
                            return true;
                    return false;
                });

                if (itr == sItemSparseStore.end())
                {
                    handler->PSendSysMessage(LANG_COMMAND_COULDNOTFIND, itemNameStr + 1);
                    handler->SetSentErrorMessage(true);
                    return false;
                }

                itemId = itr->ID;
            }
            else
                return false;
        }
        else                                                    // item_id or [name] Shift-click form |color|Hitem:item_id:0:0:0|h[name]|h|r
        {
            char const* id = handler->extractKeyFromLink((char*)args, "Hitem");
            if (!id)
                return false;
            itemId = atoul(id);
        }

        char const* ccount = strtok(NULL, " ");

        int32 count = 1;

        /*
        if (ccount)
        count = strtol(ccount, NULL, 10);

        if (count == 0)
        count = 1;
        */
        std::vector<int32> bonusListIDs;
        char const* bonuses = strtok(NULL, " ");

        // semicolon separated bonuslist ids (parse them after all arguments are extracted by strtok!)
        if (bonuses)
        {
            Tokenizer tokens(bonuses, ';');
            for (char const* token : tokens)
                bonusListIDs.push_back(atoul(token));
        }

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->getSelectedPlayer();
        playerTarget = player; /// TODO: Revork this

        TC_LOG_DEBUG("misc", handler->GetTrinityString(LANG_ADDITEM), itemId, count);

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (itemTemplate->GetInventoryType() == INVTYPE_TRINKET || itemTemplate->GetInventoryType() == INVTYPE_FINGER || itemTemplate->GetInventoryType() == INVTYPE_RELIC)
        {
            handler->PSendSysMessage("That type of item are not allowed.");
            return true;
        }

        if (itemTemplate->GetQuality() > uint8(4))
        {
            handler->PSendSysMessage("Sorry, you can not create legenaries or artifacts.");
            return true;
        }

        if (itemTemplate->GetContainerSlots() > 0)
        {
            handler->PSendSysMessage("Sorry, you can not create bags.");
            return true;
        }

        // Subtract
        if (count < 0)
        {
            playerTarget->DestroyItemCount(itemId, -count, true, false);
            handler->PSendSysMessage(LANG_REMOVEITEM, itemId, -count, handler->GetNameLink(playerTarget).c_str());
            return true;
        }

        // Adding items
        uint32 noSpaceForCount = 0;

        // check space and find places
        ItemPosCountVec dest;
        InventoryResult msg = playerTarget->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, count, &noSpaceForCount);
        if (msg != EQUIP_ERR_OK)                               // convert to possible store amount
            count -= noSpaceForCount;

        if (count == 0 || dest.empty())                         // can't add any
        {
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 craftmode = sConfigMgr->GetIntDefault("Craftmode", 0);
        uint32 cost;
        uint32 ingridientcount;

        switch (itemTemplate->GetQuality())
        {
            case 0:
                cost = sConfigMgr->GetIntDefault("Craft.MoneyCost.Gradation.Trash", 0);
                ingridientcount = sConfigMgr->GetIntDefault("Craft.ItemCost.Gradation.Trash", 1);
                break;
            case 1:
                cost = sConfigMgr->GetIntDefault("Craft.MoneyCost.Gradation.First", 1000);
                ingridientcount = sConfigMgr->GetIntDefault("Craft.ItemCost.Gradation.First", 1);
                break;
            case 2:
                cost = sConfigMgr->GetIntDefault("Craft.MoneyCost.Gradation.Second", 2000);
                ingridientcount = sConfigMgr->GetIntDefault("Craft.ItemCost.Gradation.Second", 1);
                break;
            case 3:
                cost = sConfigMgr->GetIntDefault("Craft.MoneyCost.Gradation.Third", 3000);
                ingridientcount = sConfigMgr->GetIntDefault("Craft.ItemCost.Gradation.Third", 1);
                break;
            case 4:
                cost = sConfigMgr->GetIntDefault("Craft.MoneyCost.Gradation.Fourth", 4000);
                ingridientcount = sConfigMgr->GetIntDefault("Craft.ItemCost.Gradation.Fourth", 1);
                break;
        }

        switch (craftmode)
        {
        case 0:
            if (playerTarget->GetMoney() < uint64(cost))
            {
                handler->PSendSysMessage("Sorry, you don't have enough of gold to craft that item.");
                return true;
            }
            playerTarget->ModifyMoney(-(uint64(cost)));
            break;
        case 1:
            uint32 craftingingridient;
            craftingingridient = uint32(sConfigMgr->GetIntDefault("Craft.Ingridient", 130257));
            if (!(playerTarget->HasItemCount(craftingingridient, ingridientcount, true)))
            {
                handler->PSendSysMessage("Sorry, you don't have enough of items to create that.");
                return true;
            }
            playerTarget->DestroyItemCount(craftingingridient, ingridientcount, true);
            break;
        }

        Item* item = playerTarget->StoreNewItem(dest, itemId, true, GenerateItemRandomPropertyId(itemId), GuidSet(), 0, bonusListIDs);

        // remove binding (let GM give it to another player later)
        if (player == playerTarget)
            for (ItemPosCountVec::const_iterator itr = dest.begin(); itr != dest.end(); ++itr)
                if (Item* item1 = player->GetItemByPos(itr->pos))
                    item1->SetBinding(false);

        if (count > 0 && item)
        {
            player->SendNewItem(item, count, false, true);
            if (player != playerTarget)
                playerTarget->SendNewItem(item, count, true, false);
        }

        if (noSpaceForCount > 0)
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);

        return true;
    }

};

void AddSC_custom_craft_commandscript()
{
    new custom_craft();
}

