#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Chat.h"
#include "Player.h"
#include "RBAC.h"
#include "World.h"
#include "WorldSession.h"
#include "MiscPackets.h"

class barbershop_script : public CommandScript
{
public:
    barbershop_script() : CommandScript("barbershop_script") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> commandTable =
		{
			{ "barbershop",     rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleBarbershopCommand,                "" },
		};
		return commandTable;
	}

	static bool HandleBarbershopCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		WorldPackets::Misc::EnableBarberShop packet;
		player->SendDirectMessage(packet.Write());
		return true;
	}
};


void AddSC_barbershop_commandscript()
{
	new barbershop_script();
}
