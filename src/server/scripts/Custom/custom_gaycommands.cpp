#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Chat.h"
#include "Player.h"
#include "RBAC.h"
#include "World.h"
#include "WorldSession.h"
#include "MiscPackets.h"
#include "Item.h"
#include "Log.h"
#include "Language.h"
#include "Config.h"

class gaycommands_script : public CommandScript
{
public:
	gaycommands_script() : CommandScript("gaycommands_script") { }

	std::vector<ChatCommand> GetCommands() const override
	{

		static std::vector<ChatCommand> commandTable =
		{
			{ "chroll",			rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleRollCheckerCommand,        "" },
			{ "rickroll",		rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleRollCheckerCommand,        "" },
			{ "gayfish",		rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleRollCheckerCommand,		"" },
			{ "bamboozled",     rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleRollCheckerCommand,        "" },
		};
		return commandTable;
	}

	static bool HandleRollCheckerCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->getSelectedPlayerOrSelf();

		uint32 roll = atoi(args);

		player->rollchecker = roll;

		//handler->PSendSysMessage("A value of ? was set up for ?", roll, player->GetName());

		return true;
	}

};

void AddSC_gaycommands_commandscript()
{
	new gaycommands_script();
}
