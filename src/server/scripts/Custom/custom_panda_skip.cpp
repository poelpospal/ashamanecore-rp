/*
 * Made by ConanHUN for Single Player Project - Ashamane (legion) repack
 */

#include "Player.h"

class spp_skip_pandaren : public PlayerScript
{
public:
    spp_skip_pandaren() : PlayerScript("spp_skip_pandaren") { }

    void OnLogin(Player* p_Player, bool /*firstLogin*/) override
    {
        if (p_Player->getRace() == RACE_PANDAREN_NEUTRAL)
        {
            p_Player->ShowNeutralPlayerFactionSelectUI();
//            p_Player->SetLevel(105);
        }
    }
};

void AddSC_skip_pandaren()
{
    new spp_skip_pandaren;
}
