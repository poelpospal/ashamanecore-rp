#include "ScriptMgr.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Chat.h"
#include "Player.h"
#include "RBAC.h"
#include "World.h"
#include "WorldSession.h"

class bubbles_script : public CommandScript
{
public:
	bubbles_script() : CommandScript("bubbles_script") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> typingOnOrOffCommandTable =
		{
			{ "on",         rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleTypingOn,     "" },
			{ "off",        rbac::RBAC_PERM_COMMAND_SAVE,      false, &HandleTypingOff,    "" },
		};

		static std::vector<ChatCommand> commandTable =
		{
			{ "typing",     rbac::RBAC_PERM_COMMAND_SAVE,      false, NULL,                "", typingOnOrOffCommandTable },
		};
		return commandTable;
	}


	static bool HandleTypingOn(ChatHandler* handler, const char* args)
	{
		Player* target = handler->GetSession()->GetPlayer();
		target->AddAura(149488);
		return true;
	}

	static bool HandleTypingOff(ChatHandler* handler, const char* args)
	{
		Player* target = handler->GetSession()->GetPlayer();
		target->RemoveAura(149488);
		return true;
	}

};

void AddSC_bubbles_commandscript()
{
	new bubbles_script();
}
